from django.test import TestCase, Client
from django.urls import resolve
from .views import home,experience,hasil
from .models import expu
from .apps import PesanConfig
# Create your tests here.

class TestPesan(TestCase):
    def test_event_url_is_exist(self):
        response = Client().get('/pesan/')
        self.assertEqual(response.status_code, 200)

    def test_event_index_func(self):
        found = resolve('/pesan/')
        self.assertEqual(found.func, home)

    def test_event_using_template(self):
        response = Client().get('/pesan/')
        self.assertTemplateUsed(response, 'basic.html')

class TestTambahPesan(TestCase):
    def test_add_event_url_is_exist(self):
        response = Client().get('/datas/')
        self.assertEqual(response.status_code, 200)

    def test_add_event_index_func(self):
        found = resolve('/datas/')
        self.assertEqual(found.func, experience)

    def test_add_event_using_template(self):
        response = Client().get('/datas/')
        self.assertTemplateUsed(response, 'basic.html')

    def test_event_model_create_new_object(self):
        kegiatan = expu(nama='Halim',pesan='keren')
        kegiatan.save()
        self.assertEqual(expu.objects.all().count(), 1)

    def test_event_url_post_is_exist(self):
        response = Client().post(
            '/datas/', {'nama': 'Halim','pesan': 'Keren banget'})
        self.assertEqual(response.status_code, 302)
    def test_event_url_post_is_not_valid(self):
        response = Client().post(
            '/datas/', {'pesan': 'Keren banget'})
        self.assertEqual(response.status_code, 200)
    def test_lihat_semua_hasil(self):
        response = Client().get('/res/')
        self.assertEqual(response.status_code, 200)