from django.test import TestCase, Client, tag, LiveServerTestCase
from django.urls import resolve
from .views import home, experience, prob, thanks
from .models import exp, problem
from .forms import feedbackform, problemform
from .apps import FeedbackConfig 
from selenium import webdriver
from django.http import HttpRequest



# Create your tests here.
class Testurl(TestCase):
    def test_url_tersedia(self):
        response = Client().get('/feedback/')
        self.assertEquals(response.status_code, 200)
    def test_memakai_template(self):
        response = Client().get('/feedback/')
        self.assertTemplateUsed(response, 'feedback0.html')
    def test_urlexp_tersedia(self):
        response = Client().get('/pengalaman/')
        self.assertEquals(response.status_code, 200)
    def test_memakai_templateexp(self):
        response = Client().get('/pengalaman/')
        self.assertTemplateUsed(response, 'feedback1.html')
    def test_urlprob_tersedia(self):
        response = Client().get('/masalah/')
        self.assertEquals(response.status_code, 200)
    def test_memakai_templateprob(self):
        response = Client().get('/masalah/')
        self.assertTemplateUsed(response, 'feedback2.html')

    def test_url_(self):
        response = Client().get('/save/')
        self.assertEquals(response.status_code, 200)
    def test_template_save(self):
        response = Client().get('/save/')
        self.assertTemplateUsed(response, 'thankyou.html')

class Testview(TestCase):
    def test_pengalaman(self):
        found = resolve('/pengalaman/')
        self.assertEqual(found.func, experience)

    def test_masalah(self):
        found = resolve('/masalah/')
        self.assertEqual(found.func, prob)
    
    def test_feedback(self):
        found = resolve('/feedback/')
        self.assertEqual(found.func, home)

    def test_thankyou(self):
        found = resolve('/save/')
        self.assertEqual(found.func, thanks)   
    
    def test_event_model_create_new_objectexp(self):
        pengalaman = exp(nama='raissa',pesan='semangat')
        pengalaman.save()
        self.assertEqual(exp.objects.all().count(), 1)

    def test_event_model_create_new_objectprob(self):
        masalah = problem(nama='raissa',masalah='semangat')
        masalah.save()
        self.assertEqual(problem.objects.all().count(), 1)
    
    def form_validation_accepted(self):
        form = feedbackform(data = {'nama':'raissa','pesan':'semangat'})
        self.assertTrue(form.is_valid())

    def feedform_invalid(self):
        form = feedbackform(data = {'nama':'','pesan':''})
        self.assertFalse(form.is_valid()) 
    
    def form_probform_validation(self):
        form = problemform(data = {'nama':'raissa','masalah':'semangat'})
        self.assertTrue(form.is_valid())

    def test_probForm_invalid(self):
        form = problemform(data = {'nama':'','masalah':''})
        self.assertFalse(form.is_valid())



    