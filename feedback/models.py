from django.db import models

# Create your models here.
from django.forms import ModelForm, Textarea

# Create your models here.

class exp(models.Model):
    nama = models.CharField('nama', max_length=120, null=True)
    pesan = models.TextField('pesan', max_length=250, null=True)
    
    class Meta:
        db_table = 'exp'

class problem(models.Model):
    nama = models.CharField('nama', max_length=120, null=True)
    masalah = models.TextField('masalah', max_length=250, null=True)
    
    class Meta:
        db_table = 'problem'


