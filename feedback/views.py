from django.shortcuts import render, redirect
from .forms import feedbackform, problemform
from .models import exp, problem

from django.http import HttpResponseRedirect
# Create your views here.
def home(request):
    return render(request, 'feedback0.html')
    
def experience(request):
    form = feedbackform(request.POST or None)
    if (form.is_valid() and request.method == 'POST'):
        form.save()
        return HttpResponseRedirect('/save')
    else:
        stud = exp.objects.all()
    return render(request, 'feedback1.html', {'form': form})

def prob(request):
    form = problemform(request.POST or None)
    if (form.is_valid() and request.method == 'POST'):
        form.save()
        return HttpResponseRedirect('/save')
    else:
        stud = problem.objects.all()
    return render(request, 'feedback2.html', {'form': form})

def thanks(request):
    return render(request,'thankyou.html')