from django.core import validators
from django import  forms 
from django.forms import Textarea
from .models import exp, problem

class feedbackform(forms.ModelForm):
    required_css_class = "required"
    class Meta:
        model = exp
        fields = ['nama','pesan']
        widgets ={
                'nama' : forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Tuliskan namamu disini...'}),
                'pesan' : forms.Textarea(attrs={'class': 'form-control','rows':10, 'cols':10, 'placeholder': 'Tuliskan pengalaman kamu disini'}),
        }
        labels = {
           'nama' : '',
           'pesan' : '',
        }

class problemform(forms.ModelForm):
    required_css_class = "required"
    class Meta:
        model = problem
        fields = ['nama','masalah']
        widgets ={
                'nama' : forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Tuliskan namamu disini...'}),
                'masalah' : forms.Textarea(attrs={'class': 'form-control','rows':10, 'cols':10, 'placeholder': 'Tuliskan pengalaman kamu disini'}),
        }
        labels = {
           'nama' : '',
           'masalah' : '',
        }

