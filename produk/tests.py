from django.test import TestCase, Client
from django.urls import resolve
from .views import produk, cart, updateitem
from .models import Customer, Produk, Order, Cart
# Create your tests here.
class TestProduk(TestCase):
    def test_url_produk(self):
        response = Client().get('/produk/')
        self.assertEquals(200, response.status_code)

    def test_template_used_produk(self):
        response = Client().get('/produk/')
        self.assertTemplateUsed(response, 'produk.html')
    
    def test_function_used_produk(self):
        found = resolve('/produk/')
        self.assertEqual(found.func, produk)
        
    def test_model_produk(self):
        Produk.objects.create(name="Masker", price=200)
        data_count = Produk.objects.all().count()
        self.assertEquals(data_count, 1)
        
    def test_data_produk(self):
        Produk.objects.create(name="Masker", price=200)
        Produk.objects.create(name="Ayam", price=130)
        response = Client().get('/produk/')
        isi_html = response.content.decode("utf8")

        self.assertIn("PRODUK", isi_html)
        self.assertIn("Masker", isi_html)
        self.assertIn("200.000", isi_html)
        self.assertIn("Masukkan Keranjang", isi_html)

class TestCart(TestCase):
    def test_url_cart(self):
        response = Client().get('/cart/')
        self.assertEquals(200, response.status_code)
    
    def test_template_used_produk(self):
        response = Client().get('/cart/')
        self.assertTemplateUsed(response, 'cart.html')

    def test_function_used_produk(self):
        found = resolve('/cart/')
        self.assertEqual(found.func, cart)
    
    def test_function_button(self):
        found = resolve('/updateitem/')
        self.assertEqual(found.func, updateitem)