from . import views
from django.urls import path

app_name = "artikel"

urlpatterns = [
    path('artikel/', views.artikelPage, name='home'),
    path('artikel/<slug:slug>/', views.formCom, name='post_detail'),
]