from django.db import models

# Create your models here.
STATUS = (
    (0,"Draft"),
    (1,"Publish")
)

class Post(models.Model):
    title = models.CharField(max_length=200, unique=True)
    slug = models.SlugField(max_length=200, unique=True)
    author =  models.CharField(max_length=200, unique=True)
    image = models.ImageField(null=True, blank=True)
    updated_on = models.DateTimeField(auto_now= True)
    content = models.TextField()
    created_on = models.DateTimeField(auto_now_add=True)
    status = models.IntegerField(choices=STATUS, default=0)

    @property
    def imageURL(self):
        try:
            url = self.image.url
        except :
            url = ''
        return url
    
    class Meta:
        ordering = ['-created_on']

    def __str__(self):
        return self.title

class Comment(models.Model):
    uname = models.CharField(max_length=30)
    com = models.TextField()
    post_com = models.ForeignKey(Post,on_delete=models.CASCADE,null=True)
