from django.shortcuts import render
from django.views import generic
from django.http import HttpResponseRedirect
from .forms import Form_Comment
from .models import Post, Comment
# Create your views here.
def artikelPage(request):
    post =  Post.objects.filter(status=1)
    response = {
        'object_list': post,
    }
    return render(request, 'artindex.html', response)



def formCom(request,slug):
    post = Post.objects.get(slug=slug)
    comments = Comment.objects.filter(post_com=post)
    form = Form_Comment(request.POST or None)
    if (form.is_valid and request.method == "POST"):
        new_comment = Comment(post_com=Post.objects.get(slug=slug), uname=form.data['uname'], com=form.data['com'])
        new_comment.save()
        return HttpResponseRedirect('/artikel')
    response = {
        'post': post,
        'input_form': Form_Comment,
        'comments': comments
    }
    return render(request, 'post_detail.html', response)